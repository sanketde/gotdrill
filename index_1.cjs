const got = require('./data-1.cjs')



function countAllPeople() {
  const houses = got.houses;
  let count = 0;

  houses.forEach((house) => {
    count += house.people.length;
  });
  return count;

}

function peopleByHouses() {
  const obj = {};
  const houses = got.houses;

  houses.forEach((house) => {
    obj[house.name] = house.people.length;
  });
  const sortedObj = Object.fromEntries(Object.entries(obj).sort());
  return sortedObj;

}

function everyone() {
  const houses = got.houses;
  let peoples = []
  houses.forEach((house) => {
    house.people.forEach((person) => {
      peoples.push(person.name)
    });
  });
  return peoples;
}

function nameWithS() {
  const houses = got.houses;
  let nameWithS = []
  houses.forEach((house) => {
    house.people.forEach((person) => {
      if (person.name.toLowerCase().includes('s')) {
        nameWithS.push(person.name);
      }
    });
  });
  return nameWithS;
}

function nameWithA() {
  const houses = got.houses;
  let nameWithA = []
  houses.forEach((house) => {
    house.people.forEach((person) => {
      if (person.name.toLowerCase().includes('a')) {
        nameWithA.push(person.name);
      }
    });
  });
  return nameWithA;
}

function surnameWithS() {
  const houses = got.houses;
  let surnameWithS = [];
  houses.forEach((house) => {
    house.people.forEach((person) => {

      let name = person.name;
      let surname = name.split(' ')[1];
      if (surname.startsWith('S')) {
        surnameWithS.push(name);
      }
    });
  });
  return surnameWithS;
}

function surnameWithA() {
  const houses = got.houses;
  let surnameWithA = [];
  houses.forEach((house) => {
    house.people.forEach((person) => {

      let name = person.name;
      let surname = name.split(' ')[1];
      if (surname.startsWith('A')) {
        surnameWithA.push(name);
      }
    });
  });
  return surnameWithA;
}

function peopleNameOfAllHouses() {
  const houses = got.houses;
  const res = {}
  houses.forEach((house) => {
    const houseName = house.name;
    res[houseName] = [];
    house.people.forEach((person) => {
      let personName = person.name;
      res[houseName].push(personName);

    });
  });
  const sortedObj = Object.fromEntries(Object.entries(res).sort());
  return sortedObj;

}

// Testing your result after writing your function
console.log(countAllPeople());
// Output should be 33

console.log(peopleByHouses());
// Output should be
//{Arryns: 1, Baratheons: 6, Dothrakis: 1, Freys: 1, Greyjoys: 3, Lannisters: 4,Redwyne: 1,Starks: 8,Targaryens: 2,Tullys: 4,Tyrells: 2}

console.log(everyone());
// Output should be
//["Eddard "Ned" Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon "Bran" Stark", "Rickon Stark", "Jon Snow", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Queen Cersei (Lannister) Baratheon", "King Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Balon Greyjoy", "Theon Greyjoy", "Yara Greyjoy", "Margaery (Tyrell) Baratheon", "Loras Tyrell", "Catelyn (Tully) Stark", "Lysa (Tully) Arryn", "Edmure Tully", "Brynden Tully", "Olenna (Redwyne) Tyrell", "Walder Frey", "Jon Arryn", "Khal Drogo"]

console.log(nameWithS(), 'with s');
// Output should be
// ["Eddard "Ned" Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon "Bran" Stark", "Rickon Stark", "Jon Snow", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Queen Cersei (Lannister) Baratheon", "Stannis Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Loras Tyrell", "Catelyn (Tully) Stark", "Lysa (Tully) Arryn"]

console.log(nameWithA());
// Output should be
// ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Cersei Baratheon", "Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Balon Greyjoy", "Yara Greyjoy", "Margaery Baratheon", "Loras Tyrell", "Catelyn Stark", "Lysa Arryn", "Olenna Tyrell", "Walder Frey", "Jon Arryn", "Khal Drogo"]

console.log(surnameWithS(), 'surname with s');
// Output should be
// ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Jon Snow", "Catelyn Stark"]

console.log(surnameWithA());
// Output should be
// ["Lysa Arryn", "Jon Arryn"]

console.log(peopleNameOfAllHouses());
// Output should be
// {Arryns: ["Jon Arryn"], Baratheons: ["Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon"], Dothrakis: ["Khal Drogo"], Freys: ["Walder Frey"], Greyjoys: ["Balon Greyjoy", "Theon Greyjoy", "Yara Greyjoy"], Lannisters: ["Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Cersei Baratheon"], Redwyne: ["Olenna Tyrell"], Starks: ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Jon Snow"], Targaryens: ["Daenerys Targaryen", "Viserys Targaryen"], Tullys: ["Catelyn Stark", "Lysa Arryn", "Edmure Tully", "Brynden Tully"], Tyrells: ["Margaery Baratheon", "Loras Tyrell"]}
